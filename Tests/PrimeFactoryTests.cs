﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectEuler;

namespace Tests
{
  [TestClass]
  public class PrimeFactoryTest
  {
    [TestMethod]
    [TestCategory("PrimeFactory")]
    public void PrimeFactoryGetNextTest()
    {
      PrimeFactory.Reset();
      Assert.AreEqual((ulong)2, PrimeFactory.GetNext());
      Assert.AreEqual((ulong)3, PrimeFactory.GetNext());
      Assert.AreEqual((ulong)5, PrimeFactory.GetNext());
      Assert.AreEqual((ulong)7, PrimeFactory.GetNext());
      Assert.AreEqual((ulong)11, PrimeFactory.GetNext());
      Assert.AreEqual((ulong)13, PrimeFactory.GetNext());
      Assert.AreEqual((ulong)17, PrimeFactory.GetNext());
      Assert.AreEqual((ulong)19, PrimeFactory.GetNext());
      Assert.AreEqual((ulong)23, PrimeFactory.GetNext());
    }

    [TestMethod]
    [TestCategory("PrimeFactory")]
    public void PrimeFactoryValidateFirst10000PrimesTest()
    {
      PrimeFactory.Reset();
      for (int i = 0; i < 10000; i++)
      {
        ulong prime = PrimeFactory.GetNext();
        Assert.IsTrue(ValidatePrime(prime));
      }
    }

    [TestMethod]
    [TestCategory("PrimeFactory")]
    public void PrimeFactoryGetNthTest()
    {
      PrimeFactory.Reset();
      Assert.AreEqual((ulong)2, PrimeFactory.GetNth(1));
      Assert.AreEqual((ulong)13, PrimeFactory.GetNth(6));
    }

    private bool ValidatePrime(ulong prime)
    {
      if (prime == 1 || prime == 2)
        return true;
      for (uint i = 2; i <= Math.Sqrt(prime); i++)
      {
        if (prime % i == 0)
          return false;
      }
      return true;
    }
  }
}
