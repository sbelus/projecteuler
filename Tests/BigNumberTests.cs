﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectEuler;

namespace Tests
{
  [TestClass]
  public class BigNumberTests
  {
    [TestMethod]
    [TestCategory("BigNumber")]
    public void BigNumberSimpleAddTest1()
    {
      BigNumber x = new BigNumber("900");
      BigNumber y = new BigNumber("800");
      BigNumber result = x + y;
      Assert.AreEqual("1700", result.Number);
    }

    [TestMethod]
    [TestCategory("BigNumber")]
    public void BigNumberSimpleAddTest2()
    {
      BigNumber x = new BigNumber("1");
      BigNumber y = new BigNumber("800");
      BigNumber result = x + y;
      Assert.AreEqual("801", result.Number);
    }

    [TestMethod]
    [TestCategory("BigNumber")]
    public void BigNumberSimpleAddTest3()
    {
      BigNumber x = new BigNumber("99999999");
      BigNumber y = new BigNumber("99999999");
      BigNumber result = x + y;
      Assert.AreEqual("199999998", result.Number);
    }


    [TestMethod]
    [TestCategory("BigNumber")]
    public void BigNumberSimpleAddTest4()
    {
      BigNumber x = new BigNumber("99999999");
      BigNumber y = new BigNumber("99");
      BigNumber result = x + y;
      Assert.AreEqual("100000098", result.Number);
    }

    [TestMethod]
    [TestCategory("BigNumber")]
    [ExpectedException(typeof(ArgumentException))]
    public void BigNumberWrongFormatTest1()
    {
      BigNumber x = new BigNumber("");      
    }

    [TestMethod]
    [TestCategory("BigNumber")]
    [ExpectedException(typeof(ArgumentException))]
    public void BigNumberWrongFormatTest2()
    {
      BigNumber x = new BigNumber("-1");
    }
    [TestMethod]
    [TestCategory("BigNumber")]
    [ExpectedException(typeof(ArgumentException))]
    public void BigNumberWrongFormatTest3()
    {
      BigNumber x = new BigNumber("x9129");
    }

    [TestMethod]
    [TestCategory("BigNumber")]
    public void BigNumberSimpleProductTest1()
    {
      BigNumber x = new BigNumber("120");
      BigNumber y = new BigNumber("2");
      BigNumber result = x * y;
      Assert.AreEqual("240", result.Number);
    }

    [TestMethod]
    [TestCategory("BigNumber")]
    public void BigNumberSimpleProductTest2()
    {
      BigNumber x = new BigNumber("2");
      BigNumber y = new BigNumber("120");
      BigNumber result = x * y;
      Assert.AreEqual("240", result.Number);
    }

    [TestMethod]
    [TestCategory("BigNumber")]
    public void BigNumberSimpleProductTest3()
    {
      BigNumber x = new BigNumber("1222");
      BigNumber y = new BigNumber("666");
      BigNumber result = x * y;
      Assert.AreEqual("813852", result.Number);
    }

    [TestMethod]
    [TestCategory("BigNumber")]
    public void BigNumberSimpleProductTest4()
    {
      BigNumber x = new BigNumber("32768");
      BigNumber y = new BigNumber("2");
      BigNumber result = x * y;
      Assert.AreEqual("65536", result.Number);
    }
  }
}
