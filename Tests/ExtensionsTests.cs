﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectEuler;

namespace Tests
{
  [TestClass]
  public class CheckTest
  {
    [TestMethod]
    [TestCategory("Check")]
    public void IsPalindromeTest1()
    {
      int x = 9999;
      Assert.IsTrue(Check.IsPalindrome(x));
    }

    [TestMethod]
    [TestCategory("Check")]
    public void IsPalindromeTest2()
    {
      int x = 90109;
      Assert.IsTrue(Check.IsPalindrome(x));
    }

    [TestMethod]
    [TestCategory("Check")]
    public void IsPalindromeTest3()
    {
      int x = 991;
      Assert.IsFalse(Check.IsPalindrome(x));
    }

    [TestMethod]
    [TestCategory("Check")]
    public void IsPalindromeTest4()
    {
      int x = 9987899;
      Assert.IsTrue(Check.IsPalindrome(x));
    }

    [TestMethod]
    [TestCategory("Check")]
    public void IsLeapYear1()
    {
      int x = 2000;
      Assert.AreEqual(true, Check.IsLeapYear(x));
    }

    [TestMethod]
    [TestCategory("Check")]
    public void IsLeapYear2()
    {
      int x = 2001;
      Assert.AreEqual(false, Check.IsLeapYear(x));
    }

    [TestMethod]
    [TestCategory("Check")]
    public void IsLeapYear3()
    {
      int x = 2004;
      Assert.AreEqual(true, Check.IsLeapYear(x));
    }

    [TestMethod]
    [TestCategory("Check")]
    public void IsLeapYear4()
    {
      int x = 1900;
      Assert.AreEqual(false, Check.IsLeapYear(x));
    }

  }
}
