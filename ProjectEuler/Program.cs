﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.IO;
using System.Reflection;

namespace ProjectEuler
{
  class Program
  {
    static void Main(string[] args)
    {
      TimeMeasure.Start();
      Problem36();
      Console.WriteLine(TimeMeasure.Stop());

      Console.ReadLine();
    }

    private static void Problem36()
    {
      int number = 1;
      int sum = 0;
      while (number < 1000000)
      {
        if (Check.IsPalindrome(number))
        {
          string binary = ToBinary(number);
          if (Check.IsPalindrome(binary))
          {
            sum += number;
          }
        }
        number++;
      }
      Console.WriteLine(sum);
    }

    private static string ToBinary(int number)
    {
      StringBuilder sb = new StringBuilder();
      while (number > 0)
      {
        sb.Append(number % 2);
        number /= 2;
      }
      return sb.ToString(); //we should reverse it first but for problem 36 it is not needed
    }

    private static void Problem35()
    {
      PrimeFactory.Reset();
      ulong prime = PrimeFactory.GetNext();
      int result = 0;
      while (prime < 1000000)
      {
        List<int> digits = GetDigits((int)prime);
        if (digits.Count == 1)
          result++;
        else
        {
          bool ArePrimes = true;

          for (int i = 0; i < digits.Count - 1; i++)
          {
            //rotate
            int firstDigit = digits[0];
            digits.RemoveAt(0);
            digits.Add(firstDigit);

            //check if this is prime
            int primeCandidate = Convert.ToInt32(string.Join(string.Empty, digits.ConvertAll<string>(delegate(int tmp) { return tmp.ToString(); })));
            if (!PrimeFactory.Validate((ulong)primeCandidate))
            {
              ArePrimes = false;
              break;
            }
          }
          if (ArePrimes)
          {
            result++;
          }

        }

        prime = PrimeFactory.GetNext();
      }
      Console.WriteLine(result);
    }

    private static void Problem34Parallel2()
    {
      int max = 10000000;
      int result = 0;
      object lockObj = new object();
      Parallel.ForEach(Enumerable.Range(3, max - 4), item =>
      {
        IEnumerable<int> digits = GetDigitsEnumerable(item);
        double sum = 0;
        foreach (int digit in digits)
        {
          sum += factorial(digit);
          if (sum > item)
            break;
        }
        if (item == sum)
        {
          lock (lockObj)
          {
            result += item;
          }
        }
      }
        );
      Console.WriteLine(result);
    }

    private static void Problem34Parallel()
    {
      int max = 10000000;
      int result = 0;
      object lockObj = new object();
      Parallel.ForEach(Enumerable.Range(3, max - 4), item =>
        {
          List<int> digits = GetDigits(item);
          double sum = 0;
          for (int j = 0; j < digits.Count; j++)
          {
            sum += factorial(digits[j]);
            if (sum > item)
              break;
          }
          if (item == sum)
          {
            lock (lockObj)
            {
              result += item;
            }
          }
        }
        );
      Console.WriteLine(result);
    }

    private static void Problem34()
    {
      int max = 10000000;
      int result = 0;
      for (int number = 3; number < max; number++)
      {
        List<int> digits = GetDigits(number);
        double sum = 0;
        for (int j = 0; j < digits.Count; j++)
        {
          sum += factorial(digits[j]);
          if (sum > number)
            break;
        }
        if (number == sum)
        {
          result += number;
        }
      }
      Console.WriteLine(result);
    }

    private static void Problem33()
    {
      int nValue = 1;
      int dValue = 1;
      for (int numerator = 10; numerator < 100; numerator++)
      {
        for (int denominator = numerator + 1; denominator < 100; denominator++)
        {
          List<int> nDigits = GetDigits(numerator);
          List<int> dDigits = GetDigits(denominator);
          List<int> intersect = nDigits.Intersect<int>(dDigits).ToList();
          if (intersect.Count < nDigits.Count && intersect.Count > 0)
          {
            nDigits.Remove(intersect[0]);
            dDigits.Remove(intersect[0]);

            if (IsSimplify(numerator, denominator, nDigits[0], dDigits[0]))
            {
              if (!IsTrivial(numerator, denominator, nDigits[0], dDigits[0]))
              {
                nValue *= numerator;
                dValue *= denominator;
                //int gcd = GCD(numerator, denominator);
                //product *= (denominator / gcd);
              }
            }
          }
        }
      }
      int gcd = GCD(nValue, dValue);

      Console.WriteLine(dValue / gcd);
    }

    private static int GCD(int numerator, int denominator)
    {
      if (numerator == denominator)
        return numerator;
      if (numerator > denominator)
        return GCD(numerator - denominator, denominator);
      else
        return GCD(numerator, denominator - numerator);
    }

    private static bool IsSimplify(int numerator, int denominator, int p1, int p2)
    {
      if (p2 == 0 || denominator == 0)
        return false;
      decimal org = (decimal)numerator / (decimal)denominator;
      decimal rate = (decimal)p1 / (decimal)p2;
      return org == rate;
    }

    private static bool IsTrivial(int numerator, int denominator, int p1, int p2)
    {
      if ((decimal)numerator / (decimal)10 == (decimal)p1 && (decimal)denominator / (decimal)10 == (decimal)p2)
        return true;
      else return false;
    }

    private static List<int> GetDigits(int number)
    {
      List<int> digits = new List<int>();
      while (number > 0)
      {
        digits.Add(number % 10);
        number /= 10;
      }
      digits.Reverse();
      return digits;
    }

    private static IEnumerable<int> GetDigitsEnumerable(int number)
    {
      while (number > 0)
      {
        yield return number % 10;
        number /= 10;
      }
    }

    private static void Problem32()
    {
      int[] digits = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
      int a = BuildSmallestNdigitNumber(digits, 1);
      HashSet<int> products = new HashSet<int>();
      int product = 0;
      while (a < 100 && a != -1)
      {
        int b = BuildSmallestNdigitNumber(GetOtherDigits(a), a.ToString().Length == 1 ? 4 : 3);
        while (b < 10000 && b != -1)
        {
          product = a * b;
          if (CheckAllNumbers(a, b, product))
          {
            if (!products.Contains(product))
              products.Add(product);
          }
          int[] possibleDigits = GetOtherDigits(a);
          b = IncrementNumber(b, possibleDigits);
        }
        a = IncrementNumber(a, new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 });
      }
      Console.WriteLine(products.Sum());
    }

    private static bool CheckAllNumbers(int TwoDigitNumber, int ThreeDigitNumber, int product)
    {
      int[] digits = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
      bool result = CheckIfDigitsAreOk(TwoDigitNumber, digits);
      if (!result)
        return false;
      result = CheckIfDigitsAreOk(ThreeDigitNumber, digits);
      if (!result)
        return false;
      return CheckIfDigitsAreOk(product, digits);
    }

    private static int IncrementNumber(int number, int[] possibleDigits)
    {
      int result = number;
      while (result < 10000)
      {
        result++;
        int[] copyPossibleDigits = (int[])possibleDigits.Clone();
        if (CheckIfDigitsAreOk(result, copyPossibleDigits))
          break;
      }
      if (result == 1000)
        return -1;
      return result;
    }

    private static bool CheckIfDigitsAreOk(int number, int[] possibleDigits)
    {
      foreach (char c in number.ToString().ToCharArray())
      {
        int pos = Convert.ToInt32(c.ToString()) - 1;
        if (pos < 0)
          return false; //0 digit
        if (pos >= 0 && pos <= 9)
        {
          if (possibleDigits[pos] == 1)
            possibleDigits[pos] = 0;
          else
            return false;
        }
      }
      return true;
    }

    private static int[] GetOtherDigits(int number)
    {
      int[] digits = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
      foreach (char c in number.ToString().ToCharArray())
      {
        int pos = Convert.ToInt32(c.ToString()) - 1;
        if (pos >= 0 && pos <= 9)
        {
          if (digits[pos] == 1)
            digits[pos] = 0;
          else
            throw new ArgumentException("Digits repeats");
        }
      }
      return digits;
    }

    private static int BuildSmallestNdigitNumber(int[] digits, int n)
    {
      if (n < 0 || n > 9 || digits.ToList().Sum() < n)
        throw new Exception("data error");
      int result = 0;
      int pos = 0;
      while (n > 0)
      {
        if (digits[pos] == 1)
        {
          digits[pos] = 0;
          result = result * 10 + (pos + 1);
          n--;
        }
        pos++;

      }
      return result;
    }

    private static void Problem31Other()
    {
      //Coins.
      int[] coins = { 200, 100, 50, 20, 10, 5, 2, 1 };

      //Target value.
      int target = 200;

      //Result.
      int result = combinations(coins, target, 0);

      Console.WriteLine(result);
    }

    private static int combinations(int[] coins, int target, int covered)
    {
      int comb = 0;
      int remainder = 0;

      for (int i = covered; i < coins.Length; i++)
      {
        remainder = target;
        while (remainder - coins[i] >= 0)
        {
          remainder -= coins[i];
          comb += combinations(coins, remainder, i + 1);
          if (remainder == 0) comb++;
        }
      }

      return comb;
    }

    private static int CalculateForProblem31(int[] coinsCount, int[] coins)
    {
      if (coinsCount.Length != coins.Length)
        throw new ArgumentException("lenght mismatch");
      int sum = 0;
      for (int i = 0; i < coins.Length; i++)
      {
        sum += coinsCount[i] * coins[i];
      }
      return sum;
    }

    private static void Problem30()
    {
      int sum = 0;
      int number = 2;
      while (number <= 354294) //limit could be 6 * 9^5=354294 as for six digits we can reach maximum 6*9^5
      {
        string str = number.ToString();
        int[] digits = str.ToCharArray().ToList().ConvertAll(delegate(char c)
        {
          return Convert.ToInt32(c.ToString());
        }).ToArray();

        double power = 0;
        for (int i = 0; i < digits.Length; i++)
        {
          power += Math.Pow(digits[i], 5);
        }

        if ((double)number == power)
        {
          sum += number;
        }
        number++;
      }

      Console.WriteLine(sum);
    }

    private static void Problem29()
    {
      Dictionary<BigInteger, bool> dict = new Dictionary<BigInteger, bool>();
      for (int a = 2; a <= 100; a++)
      {
        for (int b = 2; b <= 100; b++)
        {
          BigInteger item = BigInteger.Pow(new BigInteger(a), b);
          if (!dict.ContainsKey(item))
          {
            dict.Add(item, true);
          }
        }
      }
      Console.WriteLine(dict.Values.Count);
    }

    private static void Problem28()
    {
      int sum = 1;
      int size = 3;
      int prev = 1;
      while (size <= 1001)
      {
        for (int i = 0; i < 4; i++)
        {
          prev = prev + (size - 1);
          sum += prev;
        }
        size += 2;
      }
      Console.WriteLine(sum);
    }

    private static void Problem27()
    {
      PrimeFactory.Reset();
      ulong prime = PrimeFactory.GetNext();
      int maxCount = 0;
      long maxA = 0;
      long maxB = 0;
      while (prime < 1000)
      {
        for (int i = 0; i < 1000; i++)
        {
          long b = (long)prime;
          long a = (long)i;
          int count = GetCountForProblem27(a, b);
          if (count > maxCount)
          {
            maxCount = count;
            maxA = a;
            maxB = b;
          }

          count = GetCountForProblem27(-a, b);
          if (count > maxCount)
          {
            maxCount = count;
            maxA = -a;
            maxB = b;
          }

        }
        prime = PrimeFactory.GetNext();
      }
      Console.WriteLine(maxA * maxB);
      Console.WriteLine(maxCount);
    }

    private static int GetCountForProblem27(long a, long b)
    {
      int count = 0;
      long result = b;
      long n = 0;
      bool validatedPrime = result > 0 && PrimeFactory.Validate((ulong)result);
      while (validatedPrime)
      {
        count++;
        n++;
        result = n * n + a * n + b;
        validatedPrime = result > 0 && PrimeFactory.Validate((ulong)result);
      }
      return count;
    }

    private static void Problem26()
    {
      int d = 7;
      int maxCycle = 0;
      int maxCycleInt = 0;
      while (d < 1000)
      {
        List<int> list = new List<int>();
        list.Add(10);

        int next = 10;
        int div = next / d;
        int toSubtraction = d * div;
        int rest = 10 - toSubtraction;
        next = rest * 10;
        bool cycle = false;
        if (next != 0 && next != 10)
        {
          list.Add(next);
        }

        while (next != 0 && !cycle)
        {
          div = next / d;
          toSubtraction = d * div;
          rest = next - toSubtraction;
          next = rest * 10;
          if (list.Contains(next))
          {
            int cycleCount = list.Count - list.IndexOf(div) - 1;
            if (maxCycle < cycleCount)
            {
              maxCycle = cycleCount;
              maxCycleInt = d;
            }

            cycle = true;
          }
          else
            list.Add(next);
        }
        d++;
      }
      Console.WriteLine(maxCycleInt);
    }

    private static void Problem24()
    {
      int target = 1000000 - 1;
      int digits = 10;
      double sum = 0;
      bool takeNumber = true;

      IList<int> list = (IList<int>)Enumerable.Range(0, digits).ToList();
      int i = 0;
      string number = string.Empty;
      while (i < digits)
      {
        double fact = factorial(digits - i - 1);
        double count = (target - sum) / fact;
        sum += Math.Floor(count) * fact;
        int j = 0;

        if (takeNumber)
        {
          number = number + list[Convert.ToInt32(Math.Floor(count))].ToString();
          list.RemoveAt(Convert.ToInt32(Math.Floor(count)));
          takeNumber = sum < target ? true : false;
        }
        else
        {
          number += list[0].ToString();
          list.RemoveAt(0);
        }
        i++;
      }
      Console.WriteLine(number);
    }

    static object obj = new object();

    /// <summary>
    /// Parallel version works ~ 1.28sec (Intel i5-2450M). Non-parallel ~2.0 sec.
    /// </summary>
    private static void Problem23()
    {
      List<int> abundands = new List<int>();
      int sum = 0;
      Parallel.ForEach(Enumerable.Range(1, 28111), item =>
        {
          int sumDivisors = SumOfAllDivisors(item);
          if (sumDivisors > item)
          {
            lock (abundands)
            {
              abundands.Add(item);
            }
          }
        });
      //for (int number = 1; number <= 28111; number++)
      //{
      //  int sumDivisors = SumOfAllDivisors(number);
      //  if (sumDivisors > number)
      //  {
      //    abundands.Add(number);
      //  }
      //}
      abundands.Sort();
      int[] ab = abundands.ToArray();

      Console.WriteLine(TimeMeasure.Stoppage());
      List<int> excludeList = new List<int>();
      Dictionary<int, bool> excludeDict = new Dictionary<int, bool>();

      //work slower than non-parallel function due to locking dictionary object which is necessary.
      //Parallel.ForEach(Enumerable.Range(0, ab.Length - 1), item =>
      //  {
      //    for (int j = item; j < ab.Length; j++)
      //    {
      //      int added = ab[item] + ab[j];
      //      if (added > 28123)
      //        break;
      //      lock (excludeDict)
      //      {
      //        if (!excludeDict.ContainsKey(added))
      //          excludeDict.Add(added, true);
      //      }
      //    }
      //  });

      for (int i = 0; i < ab.Length; i++)
      {
        for (int j = i; j < ab.Length; j++)
        {
          int added = ab[i] + ab[j];
          if (added > 28123)
            break;
          if (!excludeDict.ContainsKey(added))
            excludeDict.Add(added, true);
        }
      }

      for (int i = 1; i <= 28123; i++)
      {
        if (!excludeDict.ContainsKey(i))
          sum += i;
      }
      if (sum != 4179871)
        Console.WriteLine("WRONG");
      Console.WriteLine(sum);
    }

    private static void Problem37()
    {
      PrimeFactory.Reset();
      ulong sum = 0;
      int i = 0;
      while (PrimeFactory.GetNext() < 7)
      { }
      while (i < 11)
      {
        ulong prime = PrimeFactory.GetNext();
        string primeStr = prime.ToString();
        bool candidate = true;
        for (int j = 1; j < primeStr.Length; j++)
        {
          string substr = primeStr.Substring(j, primeStr.Length - j);
          ulong primeCandidate = (ulong)Convert.ToInt64(substr);
          if (!PrimeFactory.Validate(primeCandidate))
            candidate = false;
        }
        if (candidate)
        {
          for (int j = primeStr.Length - 1; j > 0; j--)
          {
            string substr = primeStr.Substring(0, j);
            ulong primeCandidate = (ulong)Convert.ToInt64(substr);
            if (!PrimeFactory.Validate(primeCandidate))
              candidate = false;
          }
        }
        if (candidate)
        {
          sum += prime;
          i++;
        }
      }
      Console.WriteLine(sum);
    }

    private static void Problem25()
    {
      BigInteger x = new BigInteger(1);
      BigInteger y = new BigInteger(1);
      BigInteger z = x + y;
      int i = 3;
      while (z.ToString().Length < 1000)
      {
        x = y;
        y = z;
        z = x + y;
        i++;
      }
      Console.WriteLine(i);
    }

    private static void Problem22()
    {
      string names = Resources.names;
      string[] split = names.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
      List<string> data = split.ToList().ConvertAll(delegate(string input)
      {
        return input.Substring(1, input.Length - 2); //remove " chars
      });
      data.Sort();
      string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      List<int> sums = data.ConvertAll(delegate(string input)
      {
        int sum = 0;
        for (int i = 0; i < input.Length; i++)
        {
          sum += alphabet.IndexOf(input[i]) + 1;
        }
        return sum;
      });

      long sumAll = 0;
      checked
      {

        for (int i = 0; i < sums.Count; i++)
        {
          sumAll += (i + 1) * (long)sums[i];
        }
      }
      Console.WriteLine(sumAll);

    }

    private static void Problem21()
    {
      int sum = 0;
      for (int i = 1; i < 10000; i++)
      {
        int result = SumOfAllDivisors(i);
        if (i == result)
          continue;
        int result2 = SumOfAllDivisors(result);
        if (i == result2)
        {
          sum += i;
        }
      }
      Console.WriteLine(sum);
    }

    private static int SumOfAllDivisors(int number)
    {
      int sum = 0;
      for (int i = 1; i <= number / 2; i++)
      {
        if (number % i == 0)
        {
          sum += i;
        }
      }
      return sum;
    }

    private static void Problem19UseDateTime()
    {
      DateTime dt = new DateTime(1901, 01, 01);
      DateTime endDate = new DateTime(2000, 12, 31);
      int sum = 0;
      while (dt.CompareTo(endDate) <= 0)
      {
        if (dt.DayOfWeek == DayOfWeek.Sunday && dt.Day == 1)
          sum++;
        dt = dt.AddDays(1);
      }
      Console.WriteLine(sum);
    }

    /// <summary>
    /// this is solving problem how many sundays are in general
    /// </summary>
    private static void Problem19()
    {
      int firstDayOfNextYear = 0;
      DayOfWeek dow = DayOfWeek.Tuesday;
      int sum = 0;
      for (int i = 1901; i <= 2000; i++)
      {
        int newDayOfWeek = ((int)dow + firstDayOfNextYear) % 7;
        dow = (DayOfWeek)(newDayOfWeek);
        if (dow == DayOfWeek.Sunday)
          sum += 53;
        else
          sum += 52;
        if (Check.IsLeapYear(i))
        {
          firstDayOfNextYear = 2;
          if (dow == DayOfWeek.Saturday)
            sum++;
        }
        else
        {
          firstDayOfNextYear = 1;
        }
      }
      Console.WriteLine(sum);
    }

    private static void Problem20()
    {
      int i = 100;
      BigInteger number = new BigInteger(i);
      while (i >= 2)
      {
        i--;
        number = BigInteger.Multiply(number, new BigInteger(i));
      }
      string str = number.ToString();
      int sum = 0;
      for (int j = 0; j <= str.Length - 1; j++)
      {
        sum += Convert.ToInt32(str[j].ToString());
      }
      Console.WriteLine(sum);
    }

    /// <summary>
    /// Resolves problem 18 and 67 (which are similar)
    /// </summary>
    /// <param name="problem18">1 if want to get data for problem 18, 0 for problem 67</param>
    private static void Problem18(int problem18 = 1)
    {
      int size = 0;
      int[,] data;
      if (problem18 == 1)
        data = GetDataProblem18(ref size);
      else
      {
        data = GetDataProblem67(ref size);
      }
      //array with maximum sums
      int[,] sums = new int[size, size];
      sums[0, 0] = data[0, 0];

      //fill first column
      for (int i = 1; i < size; i++)
      {
        sums[i, 0] = sums[i - 1, 0] + data[i, 0];
      }

      //fill rest columns
      for (int j = 1; j < size; j++)
      {
        for (int i = j; i < size; i++)
        {
          int sumTop = data[i, j] + sums[i - 1, j];
          int sumLeft = data[i, j] + sums[i - 1, j - 1];
          sums[i, j] = Math.Max(sumTop, sumLeft);
        }
      }

      //find maximum from last row:
      int max = sums[size - 1, 0];
      for (int j = 1; j < size; j++)
      {
        if (max < sums[size - 1, j])
          max = sums[size - 1, j];
      }
      Console.WriteLine(max);
    }

    private static int[,] GetDataProblem67(ref int size)
    {
      string data = Resources.triangle.Replace("\n", " ");
      return GetTriangleData(ref size, data);
    }

    private static int[,] GetDataProblem18(ref int size)
    {
      string data =
        "75 "
      + "95 64 "
      + "17 47 82 "
      + "18 35 87 10 "
      + "20 04 82 47 65 "
      + "19 01 23 75 03 34 "
      + "88 02 77 73 07 63 67 "
      + "99 65 04 28 06 16 70 92 "
      + "41 41 26 56 83 40 80 70 33 "
      + "41 48 72 33 47 32 37 16 94 29 "
      + "53 71 44 65 25 43 91 52 97 51 14 "
      + "70 11 33 28 77 73 17 78 39 68 17 57 "
      + "91 71 52 38 17 14 91 43 58 50 27 29 48 "
      + "63 66 04 68 89 53 67 30 73 16 69 87 40 31 "
      + "04 62 98 27 23 09 70 98 73 93 38 53 60 04 23 ";


      return GetTriangleData(ref size, data);
    }

    private static int[,] GetTriangleData(ref int size, string data)
    {
      string[] split = data.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);


      //calculate size of array (n), where x is sum of all numbers data
      //n^2 + n = 2x
      //n^2 + n - 2x = 0 ==> a=1, b=1, c=-2x
      int delta = 1 + 4 * 2 * split.Length;
      double sqrt = Math.Sqrt(delta);
      double n = (-1 + sqrt) / 2;
      size = Convert.ToInt32(n);

      int[,] dataArray = new int[size, size];

      int i, j;
      i = j = 0;
      foreach (string number in split)
      {
        int val = int.Parse(number);
        dataArray[i, j] = val;
        j++;
        if (j > i)
        {
          j = 0;
          i++;
        }
      }
      return dataArray;
    }

    private static void Problem17()
    {
      StringBuilder all = new StringBuilder();
      string[] ones = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
                      "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
      string[] tens = { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

      string thousand = "thousand";

      int sum = 0;
      bool andNeeded = false;
      for (int i = 1; i <= 1000; i++)
      {
        StringBuilder sb = new StringBuilder();
        int tmp = i;
        while (tmp > 0)
        {
          if (tmp == 1000)
          {
            sb.Append(ones[0]);
            sb.Append(" ");
            sb.Append(thousand);
            tmp = 0;
          }
          else if (tmp >= 100)
          {
            int hundreds = tmp / 100;
            sb.Append(ones[hundreds - 1]);
            sb.Append(" ");
            sb.Append("hundred");
            tmp = tmp % 100;
            andNeeded = true;
          }
          else if (tmp >= 20)
          {
            int t = tmp / 10;
            if (sb.Length > 0)
              sb.Append(" and ");
            sb.Append(tens[t - 2]);
            tmp = tmp % 10;
            andNeeded = false;
          }
          else
          {
            if (sb.Length > 0)
              sb.Append(" ");
            if (andNeeded)
            {
              sb.Append("and ");
            }
            sb.Append(ones[tmp - 1]);
            tmp = 0;
          }
        }
        int letterCount = sb.ToString().Replace(" ", "").Length;
        sum += letterCount;
        all.AppendLine(sb.ToString());
      }


      Console.WriteLine(sum);
    }

    private static void Problem16BigInteger()
    {
      BigInteger number = BigInteger.Pow(new BigInteger(2), 1000);
      double sum = number.ToString().ToCharArray().Sum(c => char.GetNumericValue(c));
      Console.WriteLine(sum);
    }

    private static void Problem16()
    {
      BigNumber number = new BigNumber("2");
      for (int i = 2; i <= 1000; i++)
      {
        number = number * new BigNumber("2");
      }
      int sum = 0;
      for (int i = 0; i < number.Length; i++)
      {
        string digit = number.Number[i].ToString();
        int d1 = Int32.Parse(digit);
        sum += d1;
      }
      Console.WriteLine(sum);
    }

    private static void Problem15()
    {
      //double act = 40! / (20! * 20!);
      double act = 40;
      double factorial20 = factorial(20);
      double factorial40 = factorial(40);
      act = factorial40 / (factorial20 * factorial20);
      Console.WriteLine(act);
    }

    private static double factorial(int act)
    {
      double result = 1;
      for (int i = 2; i <= act; i++)
      {
        result = result * i;
      }
      return result;
    }

    private static void Problem14Other()
    {
      var cache = new Dictionary<ulong, int>();

      int max = 0;
      ulong maxNumber = 0;

      for (ulong i = 2; i < 1000000; i++)
      {
        ulong n = i;
        int terms = 1;

        while (n != 1)
        {
          n = n % 2 == 0 ? n / 2 : 3 * n + 1;

          if (cache.ContainsKey(n))
          {
            terms += cache[n];
            break;
          }

          terms++;
        }

        cache.Add(i, terms);

        if (terms > max)
        {
          max = terms;
          maxNumber = i;
        }
      }

      Console.WriteLine(maxNumber);
    }

    private static void Problem14()
    {
      ulong count = 0;
      ulong number = 1;
      ulong maxCount = 0;
      ulong maxNumber = 0;
      for (ulong i = 1; i < 1000000; i++)
      {
        checked
        {
          number = i;
          count = 1;
          while (number > 1)
          {
            if (number % 2 == 0)
            {
              number = number / 2;
            }
            else
            {
              number = number * 3 + 1;
            }
            count++;
          }
          if (maxCount < count)
          {
            maxCount = count;
            maxNumber = i;
          }
        }
      }
      Console.WriteLine(string.Format("{0} = {1}", maxNumber, count));

    }

    private static void Problem13()
    {
      BigNumber[] numbers = GetDataProblem13();
      //pierwsze dziesiec cyfr sumy stu 50-cio cyfrowych liczb
      BigNumber sum = new BigNumber("0");
      foreach (BigNumber bn in numbers)
      {
        sum = sum + bn;
      }
      Console.WriteLine(sum.Number.Substring(0, 10));
    }

    private static BigNumber[] GetDataProblem13()
    {
      string number =
  "37107287533902102798797998220837590246510135740250 "
+ "46376937677490009712648124896970078050417018260538 "
+ "74324986199524741059474233309513058123726617309629 "
+ "91942213363574161572522430563301811072406154908250 "
+ "23067588207539346171171980310421047513778063246676 "
+ "89261670696623633820136378418383684178734361726757 "
+ "28112879812849979408065481931592621691275889832738 "
+ "44274228917432520321923589422876796487670272189318 "
+ "47451445736001306439091167216856844588711603153276 "
+ "70386486105843025439939619828917593665686757934951 "
+ "62176457141856560629502157223196586755079324193331 "
+ "64906352462741904929101432445813822663347944758178 "
+ "92575867718337217661963751590579239728245598838407 "
+ "58203565325359399008402633568948830189458628227828 "
+ "80181199384826282014278194139940567587151170094390 "
+ "35398664372827112653829987240784473053190104293586 "
+ "86515506006295864861532075273371959191420517255829 "
+ "71693888707715466499115593487603532921714970056938 "
+ "54370070576826684624621495650076471787294438377604 "
+ "53282654108756828443191190634694037855217779295145 "
+ "36123272525000296071075082563815656710885258350721 "
+ "45876576172410976447339110607218265236877223636045 "
+ "17423706905851860660448207621209813287860733969412 "
+ "81142660418086830619328460811191061556940512689692 "
+ "51934325451728388641918047049293215058642563049483 "
+ "62467221648435076201727918039944693004732956340691 "
+ "15732444386908125794514089057706229429197107928209 "
+ "55037687525678773091862540744969844508330393682126 "
+ "18336384825330154686196124348767681297534375946515 "
+ "80386287592878490201521685554828717201219257766954 "
+ "78182833757993103614740356856449095527097864797581 "
+ "16726320100436897842553539920931837441497806860984 "
+ "48403098129077791799088218795327364475675590848030 "
+ "87086987551392711854517078544161852424320693150332 "
+ "59959406895756536782107074926966537676326235447210 "
+ "69793950679652694742597709739166693763042633987085 "
+ "41052684708299085211399427365734116182760315001271 "
+ "65378607361501080857009149939512557028198746004375 "
+ "35829035317434717326932123578154982629742552737307 "
+ "94953759765105305946966067683156574377167401875275 "
+ "88902802571733229619176668713819931811048770190271 "
+ "25267680276078003013678680992525463401061632866526 "
+ "36270218540497705585629946580636237993140746255962 "
+ "24074486908231174977792365466257246923322810917141 "
+ "91430288197103288597806669760892938638285025333403 "
+ "34413065578016127815921815005561868836468420090470 "
+ "23053081172816430487623791969842487255036638784583 "
+ "11487696932154902810424020138335124462181441773470 "
+ "63783299490636259666498587618221225225512486764533 "
+ "67720186971698544312419572409913959008952310058822 "
+ "95548255300263520781532296796249481641953868218774 "
+ "76085327132285723110424803456124867697064507995236 "
+ "37774242535411291684276865538926205024910326572967 "
+ "23701913275725675285653248258265463092207058596522 "
+ "29798860272258331913126375147341994889534765745501 "
+ "18495701454879288984856827726077713721403798879715 "
+ "38298203783031473527721580348144513491373226651381 "
+ "34829543829199918180278916522431027392251122869539 "
+ "40957953066405232632538044100059654939159879593635 "
+ "29746152185502371307642255121183693803580388584903 "
+ "41698116222072977186158236678424689157993532961922 "
+ "62467957194401269043877107275048102390895523597457 "
+ "23189706772547915061505504953922979530901129967519 "
+ "86188088225875314529584099251203829009407770775672 "
+ "11306739708304724483816533873502340845647058077308 "
+ "82959174767140363198008187129011875491310547126581 "
+ "97623331044818386269515456334926366572897563400500 "
+ "42846280183517070527831839425882145521227251250327 "
+ "55121603546981200581762165212827652751691296897789 "
+ "32238195734329339946437501907836945765883352399886 "
+ "75506164965184775180738168837861091527357929701337 "
+ "62177842752192623401942399639168044983993173312731 "
+ "32924185707147349566916674687634660915035914677504 "
+ "99518671430235219628894890102423325116913619626622 "
+ "73267460800591547471830798392868535206946944540724 "
+ "76841822524674417161514036427982273348055556214818 "
+ "97142617910342598647204516893989422179826088076852 "
+ "87783646182799346313767754307809363333018982642090 "
+ "10848802521674670883215120185883543223812876952786 "
+ "71329612474782464538636993009049310363619763878039 "
+ "62184073572399794223406235393808339651327408011116 "
+ "66627891981488087797941876876144230030984490851411 "
+ "60661826293682836764744779239180335110989069790714 "
+ "85786944089552990653640447425576083659976645795096 "
+ "66024396409905389607120198219976047599490197230297 "
+ "64913982680032973156037120041377903785566085089252 "
+ "16730939319872750275468906903707539413042652315011 "
+ "94809377245048795150954100921645863754710598436791 "
+ "78639167021187492431995700641917969777599028300699 "
+ "15368713711936614952811305876380278410754449733078 "
+ "40789923115535562561142322423255033685442488917353 "
+ "44889911501440648020369068063960672322193204149535 "
+ "41503128880339536053299340368006977710650566631954 "
+ "81234880673210146739058568557934581403627822703280 "
+ "82616570773948327592232845941706525094512325230608 "
+ "22918802058777319719839450180888072429661980811197 "
+ "77158542502016545090413245809786882778948721859617 "
+ "72107838435069186155435662884062257473692284509516 "
+ "20849603980134001723930671666823555245252804609722 "
+ "53503534226472524250874054075591789781264330331690 ";
      return number.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
        .ToList()
        .ConvertAll(delegate(string number2)
        {
          return new BigNumber(number2);
        }).ToArray();
    }

    private static void Problem12()
    {
      int divisors = 1;
      int nthTriangle = 1;
      int i = 1;
      int maxDivisors = 1;
      int maxDivisorsNumber = 1;
      while (divisors < 500)
      {
        checked
        {
          i++;
          nthTriangle += i;
          divisors = DivisorsCount(nthTriangle);
          if (maxDivisors < divisors)
          {
            maxDivisors = divisors;
            maxDivisorsNumber = nthTriangle;
          }
        }
      }
      Console.WriteLine(nthTriangle);
    }

    private static int DivisorsCount(int nthTriangle)
    {
      int divisors = 2;

      for (int i = 2; i <= nthTriangle / 2; i++)
      {
        if (nthTriangle % i == 0)
          divisors++;
      }
      return divisors;
    }

    /// <summary>
    /// Why this shit works?
    /// </summary>
    /// <param name="nthTriangle"></param>
    /// <returns></returns>
    private static int DivisorsCountOther(int nthTriangle)
    {
      int divisors = 0;

      for (int i = 1; i <= Math.Sqrt(nthTriangle); i++)
      {
        if (nthTriangle % i == 0)
          divisors++;
      }
      return divisors * 2;
    }

    /// <summary>
    /// Very bad performance
    /// </summary>
    /// <param name="nthTriangle"></param>
    /// <returns></returns>
    private static int DivisorsCountUsePrime(int nthTriangle)
    {
      ulong ulNthTriangle = (ulong)nthTriangle;
      int divisors = 2; //1 and nthTriangle
      List<ulong> list = new List<ulong>();
      list.Add(1);
      list.Add(ulNthTriangle);

      PrimeFactory.Reset();
      ulong prime = PrimeFactory.GetNext();
      while (prime <= ulNthTriangle / 2)
      {
        ulong i = 1;
        ulong factor = i * prime;
        while (factor <= ulNthTriangle / 2)
        {
          if (ulNthTriangle % (factor) == 0)
          {
            if (!list.Contains(factor))
            {
              divisors++;
              list.Add(factor);
            }
          }
          i++;
          factor = i * prime;
        }
        prime = PrimeFactory.GetNext();
      }
      return divisors;
    }

    private static void Problem11()
    {
      ulong[,] data = GetDataProblem11();
      ulong maxFound = 0;
      for (int i = 0; i <= 16; i++)
      {
        for (int j = 0; j <= 16; j++)
        {
          ulong product =
            data[i, j] *
            data[i, j + 1] *
            data[i, j + 2] *
            data[i, j + 3];
          maxFound = Math.Max(maxFound, product);

          product =
            data[i, j] *
            data[i + 1, j] *
            data[i + 2, j] *
            data[i + 3, j];
          maxFound = Math.Max(maxFound, product);

          product =
            data[i, j] *
            data[i + 1, j + 1] *
            data[i + 2, j + 2] *
            data[i + 3, j + 3];
          maxFound = Math.Max(maxFound, product);

          product =
            data[i + 3, j] *
            data[i + 2, j + 1] *
            data[i + 1, j + 2] *
            data[i + 0, j + 3];
          maxFound = Math.Max(maxFound, product);

        }
      }
      Console.WriteLine(maxFound);

    }

    private static ulong[,] GetDataProblem11()
    {
      ulong[,] data = new ulong[20, 20];
      string dataStr =
  "08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08 "
+ "49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00 "
+ "81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65 "
+ "52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91 "
+ "22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80 "
+ "24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50 "
+ "32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70 "
+ "67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21 "
+ "24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72 "
+ "21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95 "
+ "78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92 "
+ "16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57 "
+ "86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58 "
+ "19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40 "
+ "04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66 "
+ "88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69 "
+ "04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36 "
+ "20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16 "
+ "20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54 "
+ "01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48 ";
      string[] split = dataStr.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
      int i = 0, j = 0;
      foreach (string number in split)
      {
        data[i, j] = ulong.Parse(number);

        j++;
        if (j == 20)
        {
          j = 0;
          i++;
        }
      }
      return data;
    }

    private static void Problem10()
    {
      checked
      {
        PrimeFactory.Reset();
        ulong actualPrime = PrimeFactory.GetNext();
        ulong sum = 0;
        while (actualPrime < 2000000)
        {
          sum += actualPrime;
          actualPrime = PrimeFactory.GetNext();
        }
        Console.WriteLine(sum);
      }

    }

    private static void Problem9()
    {
      for (ulong a = 0; a <= 1000; a++)
      {
        for (ulong b = a + 1; b <= 1000; b++)
        {
          for (ulong c = b + 1; c <= 1000; c++)
          {
            if (a + b + c > 1000)
              break;
            if (a + b + c == 1000)
            {
              if (Math.Pow(a, 2) + Math.Pow(b, 2) == Math.Pow(c, 2))
                Console.WriteLine(string.Format("{0}*{1}*{2}={3}", a, b, c, a * b * c));
            }
          }
        }
      }
    }

    private static void Problem8()
    {
      string bigNumber = "7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450";

      int maxFound = -1;
      for (int i = 0; i < bigNumber.Length - 5; i++)
      {
        int d1 = Convert.ToInt32(bigNumber[i].ToString());
        int d2 = Convert.ToInt32(bigNumber[i + 1].ToString());
        int d3 = Convert.ToInt32(bigNumber[i + 2].ToString());
        int d4 = Convert.ToInt32(bigNumber[i + 3].ToString());
        int d5 = Convert.ToInt32(bigNumber[i + 4].ToString());

        int candidate = d1 * d2 * d3 * d4 * d5;
        if (candidate > maxFound)
          maxFound = candidate;
      }
      Console.WriteLine(maxFound);
    }

    private static void Problem7()
    {
      Console.WriteLine(PrimeFactory.GetNth(10001));
    }

    private static void Problem6()
    {
      ulong sum = 0;
      for (ulong i = 1; i <= 100; i++)
      {
        for (ulong j = i + 1; j <= 100; j++)
        {
          sum += 2 * i * j;
        }
      }
      Console.WriteLine(sum);
    }

    private static void Problem5()
    {
      ulong step = 20;
      ulong x = step;
      while (!Check.IsEvenlyDivisible(x, step))
      {
        x += step;
      }
      Console.WriteLine(x);
    }

    private static void Problem4()
    {
      int maxFound = -1;
      int iter = 0;
      for (int x = 999; x >= 100; x--)
      {
        if (maxFound != -1 && x * 999 < maxFound)
          break;
        for (int y = 999; y >= 100; y--)
        {
          iter++;
          int product = x * y;
          if (maxFound != -1 && product < maxFound)
            break;
          if (Check.IsPalindrome(product))
          {
            if (maxFound == -1 || product > maxFound)
            {
              maxFound = product;
              break;
            }
          }
        }
      }
      Console.WriteLine(maxFound);
      Console.WriteLine(iter);
    }

    private static void Problem4Other()
    {
      Tuple<int, int, int> biggest = Tuple.Create(0, 0, 0);

      for (int i = 999; i >= 100; i--)
      {
        for (int j = 999; j >= 100; j--)
        {
          int prod = i * j;
          if (IsPalindrome(prod))
          {
            //Console.WriteLine("{0,00} {1:00} {2}", i, j, prod);
            if (prod > biggest.Item3)
            {
              biggest = Tuple.Create(i, j, prod);
            }
          }
        }
      }

      //Console.WriteLine();
      Console.WriteLine("Biggest is {0} x {1} = {2}", biggest.Item1, biggest.Item2, biggest.Item3);
      //Console.ReadKey(true);
    }

    private static bool IsPalindrome(long number)
    {
      string text = number.ToString(CultureInfo.InvariantCulture);

      return text.Reverse().SequenceEqual(text);
    }

    private static void Problem3()
    {
      List<ulong> primes = new List<ulong>();
      ulong x = 600851475143;
      while (x > 1)
      {
        ulong prime = PrimeFactory.GetNext();
        if (x % prime == 0)
        {
          x = x / prime;
          primes.Add(prime);
        }
      }
      Console.WriteLine(string.Join(",", primes));
    }

    private static void Problem2()
    {
      ulong x = 1;
      ulong y = 2;
      ulong sum = 2;
      ulong iFibo = 3;
      while (iFibo < 4000000)
      {
        if (iFibo % 2 == 0)
          sum += iFibo;
        x = y;
        y = iFibo;
        iFibo = x + y;
      }
      Console.WriteLine(sum);
    }

    private static void Problem1()
    {
      int sum = 0;
      for (int i = 1; i < 1000; i++)
      {
        if (i % 3 == 0 || i % 5 == 0)
          sum += i;
      }
      Console.WriteLine(sum);
    }

    //private static void Problem2Wrong()
    //{
    //  BigNumber x = new BigNumber("1");
    //  BigNumber y = new BigNumber("2");

    //  BigNumber sum = y;
    //  for (ulong i = 3; i < 4000000; i++)
    //  {
    //    if (i % 1000 == 0)
    //    {
    //      Console.WriteLine(string.Format("progress {0}", i));
    //      Console.WriteLine(TimeMeasure.Stoppage());
    //    }
    //    BigNumber iFibo = x + y;
    //    if (iFibo.Even())
    //      sum += iFibo;

    //    x = y;
    //    y = iFibo;
    //  }
    //  Console.WriteLine(sum);
    //}

  }
}
