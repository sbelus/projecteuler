﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler
{
  public class PrimeFactory
  {
    static ulong actualPrime = 1;

    public static void Reset()
    {
      actualPrime = 1;
    }

    public static ulong GetNext()
    {
      if (actualPrime == 1 || actualPrime == 2)
        actualPrime++;
      else
        actualPrime += 2;

      while (!Validate())
      {
        actualPrime += 2;
      }
      return actualPrime;
    }

    public static ulong GetNth(ulong n)
    {
      Reset();
      ulong prime = 1;
      for (ulong i = 1; i <= n; i++)
      {
        prime = GetNext();
      }
      return prime;
    }

    private static bool Validate()
    {
      return Validate(actualPrime);
    }

    public static bool Validate(ulong primeCandidate)
    {
      if (primeCandidate == 1)
        return false;
      if (primeCandidate == 2)
        return true;
      bool isPrime = true;
      for (ulong i = 2; i <= Math.Sqrt(primeCandidate); i++)
      {
        if (primeCandidate % i == 0)
        {
          isPrime = false;
          break;
        }
      }
      return isPrime;
    }

  }
}
