﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler
{
  public static class TimeMeasure
  {
    static DateTime? start = null;

    public static void Start()
    {
      start = DateTime.Now;
    }

    public static string Stop()
    {
      DateTime now = DateTime.Now;
      if (!start.HasValue)
        throw new Exception("Najpierw uruchom start");
      TimeSpan ts = now - start.Value;
      start = null;
      return string.Format("Time: {0}", ts.ToString());
    }


    internal static string Stoppage()
    {
      DateTime now = DateTime.Now;
      if (!start.HasValue)
        throw new Exception("Najpierw uruchom start");
      TimeSpan ts = now - start.Value;
      return string.Format("Time: {0}", ts.ToString());
    }
  }

  public static class Check
  {
    public static bool IsPalindrome(int product)
    {
      string str1 = product.ToString();
      return str1.Reverse().SequenceEqual(str1);
    }

    public static bool IsPalindrome(string str)
    {
      return str.SequenceEqual(str.Reverse());
    }

    public static bool IsEvenlyDivisible(ulong x, ulong step)
    {
      for (ulong i = 2; i <= step; i++)
      {
        if (x % i != 0)
          return false;
      }
      return true;
    }

    public static bool IsLeapYear(int year)
    {
      if (year % 400 == 0)
        return true;
      else if (year % 100 == 0)
        return false;
      else if (year % 4 == 0)
        return true;
      else
        return false;
    }
  }
}
