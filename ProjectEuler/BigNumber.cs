﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProjectEuler
{
  public class BigNumber
  {
    string num = "0";
    int len = 1;

    public string Number
    {
      get
      {
        return num;
      }
    }

    public int Length
    {
      get
      {
        return len;
      }
    }

    public BigNumber(string number)
    {
      if (!Validate(number))
        throw new ArgumentException("Błędny format liczby");
      num = number;
      len = number.Length;
    }

    private bool Validate(string number)
    {
      Regex regex = new Regex(@"^\d+$");
      return regex.IsMatch(number);
    }


    
    public static BigNumber operator +(BigNumber x, BigNumber y)
    {
      BigNumber bigger = x;
      BigNumber lower = y;
      if (x.Length < y.Length)
      {
        bigger = y;
        lower = x;
        lower.Pad(bigger.Length);
      }
      else
      {
        lower.Pad(bigger.Length);
      }
      StringBuilder sb = new StringBuilder();
      int rest = 0;
      for (int i = bigger.Length - 1; i >= 0; i--)
      {
        string digit1 = "0"; 
        string digit2 = "0";  
        
        digit1 = bigger.Number[i].ToString();
        if (i < lower.Length)
          digit2 = lower.Number[i].ToString();

        int d1 = int.Parse(digit1);
        int d2 = int.Parse(digit2);
        int sum = d1 + d2 + rest;
        rest = sum >= 10 ? 1 : 0;
        sum = sum % 10;
        sb.Insert(0, sum.ToString());
      }
      if (rest > 0)
        sb.Insert(0, rest.ToString());
      return new BigNumber(sb.ToString());
    }

    public static BigNumber operator *(BigNumber x, BigNumber y)
    {
      BigNumber bigger = x;
      BigNumber lower = y;
      if (x.Length < y.Length)
      {
        bigger = y;
        lower = x;
        lower.Pad(bigger.Length);
      }
      else
      {
        lower.Pad(bigger.Length);
      }
      BigNumber sum = new BigNumber("0");
      
      for (int i = lower.Length - 1; i >= 0; i--)
      {
        StringBuilder sb = new StringBuilder();
        int rest = 0;
        int padRight = lower.Length - 1 - i;
        if (padRight > 0)
          sb.Append(string.Empty.PadRight(padRight, '0'));
        for (int j = bigger.Length - 1; j >= 0; j--)
        {
          string digit1 = "0";
          string digit2 = "0";

          digit1 = lower.Number[i].ToString();
          if (digit1 == "0")
            break;
          digit2 = bigger.Number[j].ToString();
          
          int d1 = int.Parse(digit1);
          int d2 = int.Parse(digit2);
          int product = d1 * d2 + rest;
          rest = product / 10;
          product = product % 10;
          sb.Insert(0, product.ToString());
        }
        if (rest > 0)
          sb.Insert(0, rest.ToString());
        sum = sum + new BigNumber(sb.ToString());
      }
      return sum;
    }

    public bool Even()
    {
      char last = Number[Number.Length - 1];
      return last == '0' || last == '2' || last == '4' || last == '6' || last == '8';
    }

    public void Pad(int toLength)
    {
      this.num = num.PadLeft(toLength, '0');
      this.len = num.Length;
    }

    public override string ToString()
    {
      return num;
    }
  }
}
